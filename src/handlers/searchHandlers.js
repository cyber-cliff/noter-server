const searchDBBridge = require("../db/searchBridge");
const tagDBBridge = require("../db/tagBridge");
const usersDBBridge = require("../db/usersBridge");

const errors = require("../api/errors");

const noteCasters = require("../casters/noteCasters");
const userCasters = require("../casters/userCasters");
const tagCasters = require("../casters/tagCasters");

module.exports = class {
    static async search({phrase, users, published_notes, user_notes}, auth) {
        const results = {};

        if (users) {
            results.users = await module.exports.searchUsers(phrase);
        }
        if (published_notes || user_notes) {
            if (user_notes && !auth) {
                throw(errors.authFailed());
            }

            let userId;
            if (user_notes) {
                userId = auth.user_id;
            }

            const notesAndTagsResults = await module.exports.searchNotes(phrase, published_notes, userId);
            if (published_notes) {
                results.published_notes = notesAndTagsResults.publishedNotes;
            }
            if (user_notes) {
                results.user_notes = notesAndTagsResults.userNotes;
            }

            results.tags = notesAndTagsResults.tags;
            results.authors = notesAndTagsResults.authors;
        }

        return {
            data: results
        };
    }

    static async searchUsers(phrase) {
        let users = await searchDBBridge.searchUsers(phrase);

        users = users.map(user => {
            return userCasters.userProfile(user);
        });

        return users;
    }

    static async searchNotes(phrase, search_published, userId) {
        let publishedNotes = [], userNotes = [];
        if (search_published) {
            publishedNotes = await searchDBBridge.searchNotes(phrase);
        }

        if (userId) {
            userNotes = await searchDBBridge.searchUserNotes(phrase, userId);
        }

        const tagIdsUsed = [];
        const authorIdsUsed = [];

        publishedNotes = publishedNotes.map(note => {
            for (const tagId of note.tags) {
                if (!tagIdsUsed.includes(tagId)) {
                    tagIdsUsed.push(tagId);
                }
            }

            if (!authorIdsUsed.includes(note.author)) {
                authorIdsUsed.push(note.author);
            }

            return noteCasters.noteThumbnail(note);
        });
        userNotes = userNotes.map(note => { // TODO: duplicate code
            for (const tagId of note.tags) {
                if (!tagIdsUsed.includes(tagId)) {
                    tagIdsUsed.push(tagId);
                }
            }

            if (!authorIdsUsed.includes(note.author)) {
                authorIdsUsed.push(note.author);
            }

            return noteCasters.noteThumbnail(note);
        });

        let tags = await tagDBBridge.getTagsById(tagIdsUsed);
        tags = tags.map(tag => {
            return tagCasters.tag(tag);
        });

        let authors = await usersDBBridge.getUsersById(authorIdsUsed);
        authors = authors.map(author => {
            return userCasters.userProfile(author);
        });

        return {
            publishedNotes,
            userNotes,
            tags,
            authors
        };
    }
};
