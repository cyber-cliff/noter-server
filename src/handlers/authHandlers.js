const bcrypt = require("bcrypt");

const usersDBBridge = require("../db/usersBridge");

const authService = require("../services/auth");

const errors = require("../api/errors");

const registrationUsernamesBuffer = []; // Store the usernames to prevent temporary duping while we insert user into db

module.exports = class {
    static async login({ username, key, is_persistent_token, generate_persistent_token }, auth, req) {
        const ua = req.header('User-Agent');

        //* Get user object
        const user = await usersDBBridge.getUserByUsername(username);

        if (!user) {
            throw (errors.userNotExist());
        }


        //* Check if key is valid
        let keyValid = false;

        if (is_persistent_token) {
            generate_persistent_token = false; // Don't ever generate PT from PT
            for (const storedPT of user.persistent_tokens) {
                if (await bcrypt.compare(key, storedPT.hash)) {
                    keyValid = true;
                    break;
                }
            }
        } else { // Normal username & password login
            keyValid = await bcrypt.compare(key, user.password_hash);
        }

        if (!keyValid) {
            throw (errors.invalidKey());
        }

        if (user.restricted) {
            if (user.restricted.banned > 0) {
                if (user.restricted.banned === Infinity) {
                    throw (errors.userBanned(Infinity));
                }
            }
        }

        const sessionData = await authService.storeSession({
            userId: user._id.toString(),
            ua,
            generate_persistent_token
        });

        return {
            data: {
                token: sessionData.token,
                user_id: user._id.toString(),
                persistent_token: sessionData.generatedPT
            }
        };
    }

    static async register({username, password}) {
        const existingUser = await usersDBBridge.getUserByUsername(username);

        if (existingUser) {
            throw(errors.userExists());
        }

        if (registrationUsernamesBuffer.includes(username)) {
            throw errors.userExists();
        } else {
            registrationUsernamesBuffer.push(username);
        }

        const password_hash = await bcrypt.hash(password, 12);

        const user = {
            username,
            password_hash,
            description: "",
            persistent_tokens: [],
            badges: [],
            saved_notes: [],
            created_at: new Date().getTime(),
            active_at: new Date().getTime()
        };

        await usersDBBridge.registerUser(user);

        return {
            data: {}
        };
    }

    static async validateSession({ user_id, token }, auth, req) {
        const ua = req.header('User-Agent');

        return {
            data: {
                valid: authService.validateSession({
                    user_id,
                    token,
                    ua
                }, auth.disable_active_time)
            }
        };
    }

    // Destroys all sessions along with all persistent tokens
    static async destroySessions(data, auth, req) {
        authService.deleteSessionByUserId(auth.user_id);

        await usersDBBridge.deauth(auth.user_id);

        return {
            data: {}
        };
    }
};