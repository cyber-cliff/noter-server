/*

*   All methods relating to users

*/

const usersDBBridge = require("../db/usersBridge");
const notesDBBridge = require("../db/notesBridge");
const tagDBBridge = require("../db/tagBridge");

const userCasters = require("../casters/userCasters");
const noteCasters = require("../casters/noteCasters");
const tagCasters = require("../casters/tagCasters");

const errors = require("../api/errors");

const bookmarkedNotesBuffer = {}; // userId: Array<noteId>

module.exports = class {
    static async getUserProfile({user_id}) {
        const users = await usersDBBridge.getUsersById([user_id]);
        const user = users[0];

        return {
            data: userCasters.userProfile(user)
        };
    }

    static async getUserNotes({user_id, tag_ids}, auth) {
        let isUserOwner = false;
        if (auth) {
            if (user_id === auth.user_id) {
                isUserOwner = true;
            }
        }

        if (!tag_ids) {
            tag_ids = []; // If no tags were specified, we still need an empty array
        }

        let notes = await notesDBBridge.getNotesByUserId(user_id, isUserOwner, tag_ids, true);

        const castedNotesAndTags = await module.exports.getCastedNotesAndTags(notes);

        return {
            data: {
                notes: castedNotesAndTags.notes,
                tags: castedNotesAndTags.tags
            }
        };
    }

    static async getSavedNotes(data, auth) {
        const user_id = auth.user_id;

        const users = await usersDBBridge.getUsersById([user_id]);
        const user = users[0];

        let notes = await notesDBBridge.getNotesById(user.saved_notes, true);
        const tagIdsUsed = [];

        notes = notes.map(note => {
            for (const tagId of note.tags) {
                if (!tagIdsUsed.includes(tagId)) {
                    tagIdsUsed.push(tagId);
                }
            }
            return noteCasters.noteThumbnail(note);
        });

        let tags = await tagDBBridge.getTagsById(tagIdsUsed);
        tags = tags.map(tag => {
            return tagCasters.tag(tag);
        });

        return {
            data: {
                notes,
                tags
            }
        };
    }

    static async bookmarkNote({note_id}, auth) {
        const user_id = auth.user_id;

        if (bookmarkedNotesBuffer[user_id]) {
            if (bookmarkedNotesBuffer[user_id].includes(note_id)) {
                throw errors.noteAlreadyBookmarked();
            }
        } else {
            bookmarkedNotesBuffer[user_id] = [];
        }

        bookmarkedNotesBuffer[user_id].push(note_id);

        const users = await usersDBBridge.getUsersById([user_id]);
        const user = users[0];

        if (user.saved_notes.includes(note_id)) {
            throw errors.noteAlreadyBookmarked();
        }

        await usersDBBridge.pushBookmarkedNote(user_id, note_id);

        return {
            data: {}
        };
    }

    static async unbookmarkNote({note_id}, auth) {
        const user_id = auth.user_id;

        if (bookmarkedNotesBuffer[user_id]) {
            if (bookmarkedNotesBuffer[user_id].includes(note_id)) {
                bookmarkedNotesBuffer[user_id] = bookmarkedNotesBuffer[user_id].filter(storedNoteId => {
                    return storedNoteId !== note_id;
                });

                console.log(bookmarkedNotesBuffer);
            }
        }

        await usersDBBridge.removeBookmarkedNote(user_id, note_id);

        return {
            data: {}
        };
    }

    static async getCastedNotesAndTags(notes) {
        const tagIdsUsed = [];

        notes = notes.map(note => {
            for (const tagId of note.tags) {
                if (!tagIdsUsed.includes(tagId)) {
                    tagIdsUsed.push(tagId);
                }
            }
            return noteCasters.noteThumbnail(note);
        });

        let tags = await tagDBBridge.getTagsById(tagIdsUsed);
        tags = tags.map(tag => {
            return tagCasters.tag(tag);
        });

        return {
            notes,
            tags
        };
    }
};
