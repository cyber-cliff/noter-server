/*

*   All methods relating to notes

*/

const usersDBBridge = require("../db/usersBridge");
const notesDBBridge = require("../db/notesBridge");
const tagDBBridge = require("../db/tagBridge");

const userCasters = require("../casters/userCasters");
const noteCasters = require("../casters/noteCasters");
const tagCasters = require("../casters/tagCasters");

const errors = require("../api/errors");

module.exports = class {
    static async createNote({ title, type }, auth) {
        if (!['doc'].includes(type)) {
            type = 'doc';
        }

        let body = {};
        switch (type) {
            case 'doc':
                body = {
                    sections: [
                        {
                            type: 'markdown',
                            content: '# Hello, World!'
                        }
                    ]
                };
                break;
        }

        const note = {
            author: auth.user_id,
            title: title || "Untitled_" + new Date().getTime(),
            tags: [],
            doc: {
                head: {
                    ndfVersion: 4,
                    type
                },
                body
            },
            created_at: new Date().getTime(),
            modified_at: new Date().getTime(),
            access: 2, // 0=private, 1=invisible 2=not_published 3=published
        };

        const note_id = await notesDBBridge.createNote(note);

        return {
            data: {
                note_id
            }
        };
    }

    static async getNote({ note_id }, auth) {

        const notes = await notesDBBridge.getNotesById([note_id]);
        let note = notes[0];

        if (!note) {
            throw (errors.noteNotExist());
        }

        note = noteCasters.note(note);

        if (note.access === 0) {
            if (!auth) {
                throw (errors.noteNotExist());
            }
            if (auth.user_id !== note.author) {
                throw (errors.noteNotExist());
            }
        }

        const users = await usersDBBridge.getUsersById([note.author]);
        const user = userCasters.userProfile(users[0]);

        let tags = await tagDBBridge.getTagsById(note.tags);
        tags = tags.map(tag => {
            return tagCasters.tag(tag);
        });

        return {
            data: {
                note,
                tags,
                users: [user],
            }
        };
    }

    static async update({ note_id, note }, auth) {

        const oldNotes = await notesDBBridge.getNotesById([note_id]);
        let oldNote = oldNotes[0];

        if (!oldNote) {
            throw (errors.noteNotExist());
        }

        if (oldNote.access === 0) {
            if (!auth) {
                throw (errors.noteNotExist());
            }
            if (auth.user_id !== oldNote.author) {
                throw (errors.noteNotExist());
            }
        }

        if (auth.user_id !== oldNote.author) {
            throw (errors.accessDenied());
        }

        if (note.doc) {
            if (note.doc.head.encryption && oldNote.access > 1) {
                throw (errors.encryptionOnPublic());
            }
            oldNote.doc = note.doc;
            oldNote.modified_at = new Date().getTime();
        }

        if (note.title) oldNote.title = note.title;

        if (note.tags) oldNote.tags = note.tags;

        if (typeof(note.access) === 'number') {
            if (oldNote.doc.head.encryption && note.access > 1) {
                throw (errors.encryptionOnPublic());
            }
            oldNote.access = note.access;
        }

        if (JSON.stringify(oldNote).length > 100000) { // TODO: hardcoded for now :/
            throw (errors.tooLarge());
        }

        await notesDBBridge.updateNote(note_id, oldNote);

        return {
            data: {
                modified_at: oldNote.modified_at
            }
        };
    }

    static async delete({ note_id }, auth) {

        const notes = await notesDBBridge.getNotesById([note_id]);
        let note = notes[0];

        if (!note) {
            throw (errors.noteNotExist());
        }

        if (note.access === 0) {
            if (!auth) {
                throw (errors.noteNotExist());
            }
            if (auth.user_id !== note.author) {
                throw (errors.noteNotExist());
            }
        }

        if (auth.user_id !== note.author) {
            throw (errors.accessDenied());
        }

        await notesDBBridge.deleteNote(note_id);

        return {
            data: {}
        };
    }
};
