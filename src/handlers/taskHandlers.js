
const tasksBridge = require('../db/tasksBridge');
const tagBridge = require('../db/tagBridge');

const taskCasters = require("../casters/taskCasters");
const tagCasters = require("../casters/tagCasters");

module.exports = class {
    static async createTask({ name, timestamp }, auth) {
        const task = {
            author: auth.user_id,
            name,
            timestamp,
            completed: false,
            tags: []
        };

        const task_id = await tasksBridge.createTask(task);

        return {
            data: {
                task_id
            }
        };
    }

    static async getUserTasks(data, auth) {
        const userId = auth.user_id;

        let tasks = await tasksBridge.getUserTasks(userId);
        const tagIdsUsed = [];

        tasks = tasks.map(task => {
            for (const tagId of task.tags) {
                if (!tagIdsUsed.includes(tagId)) {
                    tagIdsUsed.push(tagId);
                }
            }
            return taskCasters.task(task);
        });

        let tags = await tagBridge.getTagsById(tagIdsUsed);
        tags = tags.map(tag => {
            return tagCasters.tag(tag);
        });

        return {
            data: {
                tasks,
                tags
            }
        };
    }

    static async update({ task_id, task }, auth) {

        const oldTasks = await tasksBridge.getTasksById([task_id]);
        let oldTask = oldTasks[0];

        if (!oldTask) {
            throw (errors.noteNotExist()); // TODO: more general 404 error msg
        }

        if (auth.user_id !== oldTask.author) {
            throw (errors.accessDenied());
        }

        if (task.completed !== undefined) oldTask.completed = task.completed;

        await tasksBridge.updateTask(task_id, oldTask);

        return {
            data: {}
        };
    }

    static async deleteTask({task_id}, auth) {
        const tasks = await tasksBridge.getTasksById([task_id]);
        let task = tasks[0];

        if (!task) {
            throw (errors.noteNotExist()); // TODO: generalalla
        }

        if (auth.user_id !== task.author) {
            throw (errors.accessDenied());
        }

        await tasksBridge.deleteTask(task_id);

        return {
            data: {}
        };
    }
};