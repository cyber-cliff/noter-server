
const errors = require("../api/errors");

const vCode = 20052501;

module.exports = class {
    static async getInfo() {
        return {
            data: {
                status: "online",
                version: {
                    string: module.exports.makeVersionString(vCode, 'alpha'),
                    code: vCode
                }
            }
        };
    }

    static makeVersionString(vCode, phase) {
        const vcstr = '' + vCode;
    
        const year  = parseInt(vcstr[0] + vcstr[1], 10);
        const month = parseInt(vcstr[2] + vcstr[3], 10);
        const day   = parseInt(vcstr[4] + vcstr[5], 10);
        const patch = parseInt(vcstr[6] + vcstr[7], 10);
    
        const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
    
        return `${phase}${year}.${month}.${day}${(patch > 1) ? alphabet[patch - 1] : ''}`;
    }
};
