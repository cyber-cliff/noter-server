
const statsBridge = require('../db/statsBridge');
const statsService = require('../services/stats');
const statCasters = require('../casters/statCasters');

module.exports = class {
    static async getStats() {
        let stats = await statsBridge.getStats(24);

        stats = stats.map(statEntry => {
            return statCasters.statEntry(statEntry);
        });

        return {
            data: {
                stats
            }
        };
    }
};