
const errors = require("../api/errors");

const notesDBBridge = require("../db/notesBridge");
const tagDBBridge = require("../db/tagBridge");

const tagCasters = require("../casters/tagCasters");

module.exports = class {
    static async get(data, auth) {
        let tags = await tagDBBridge.getTagsByUserId(data.user_id || auth.user_id);

        tags = tags.map(tag => {
            return tagCasters.tag(tag);
        });

        return {
            data: {
                tags
            }
        };
    }
    
    static async create({name, color}, auth) {
        let newTag = {
            author: auth.user_id,
            name,
            color
        };

        let tag = await tagDBBridge.createTag(newTag);
        tag = tagCasters.tag(tag);

        return {
            data: {
                tag
            }
        };
    }

    static async delete({tag_id}, auth) {
        const tags = await tagDBBridge.getTagsById([tag_id]);
        const tag = tags[0];

        if (!tag) {
            throw (errors.tagNotExist());
        }
        
        if (tag.author !== auth.user_id) {
            throw (errors.tagNotExist());
        }

        await notesDBBridge.deleteTagFromNotes(tag_id);
        await tagDBBridge.deleteTag(tag_id);

        return {
            data: {}
        };
    }
};
