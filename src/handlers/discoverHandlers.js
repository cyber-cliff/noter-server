const notesDBBridge = require("../db/notesBridge");
const tagDBBridge = require("../db/tagBridge");

const noteCasters = require("../casters/noteCasters");
const tagCasters = require("../casters/tagCasters");

const errors = require("../api/errors");

module.exports = class {
    static async publishedNotes(data, auth, req) {
        let notes = await notesDBBridge.getRecentPublishedNotes();

        const tagIdsUsed = [];
        notes = notes.map(note => {
            for (const tagId of note.tags) {
                if (!tagIdsUsed.includes(tagId)) {
                    tagIdsUsed.push(tagId);
                }
            }
            return noteCasters.noteThumbnail(note);
        });

        let tags = await tagDBBridge.getTagsById(tagIdsUsed);
        tags = tags.map(tag => {
            return tagCasters.tag(tag);
        });

        return {
            data: {
                notes,
                tags
            }
        };
    }
    static async newsNotes(data, auth, req) {
        let notes = await notesDBBridge.getRecentNewsNotes();

        const tagIdsUsed = [];
        notes = notes.map(note => {
            for (const tagId of note.tags) {
                if (!tagIdsUsed.includes(tagId)) {
                    tagIdsUsed.push(tagId);
                }
            }
            return noteCasters.noteThumbnail(note);
        });

        let tags = await tagDBBridge.getTagsById(tagIdsUsed);
        tags = tags.map(tag => {
            return tagCasters.tag(tag);
        });

        return {
            data: {
                notes,
                tags
            }
        };
    }
};