
const requests = {
    count: 0
};

module.exports.mv = function (options) {
    return function (req, res, next) {
        if (req.body.diag) {
            // TODO: ???
        }
        requests.count++;
        console.log('Request count inc to ' + requests.count);
        
        next();
    };
};

module.exports.getStats = function() {
    return requests;
};

module.exports.resetStats = function() {
    requests.count = 0;
};