module.exports = class {
    static internalError(e) {
        return {
            msg: "Internal server error. If you want to help fix this error, please open an issue on the backend repo.",
            code: 0,
            e
        };
    }

    static missingDetails(propName) {
        return {
            msg: "Missing / incomplete data ('" + propName + "'). Consult docs for required information.",
            code: 1
        };
    }

    static userNotExist() {
        return {
            msg: "User does not exist.",
            code: 2
        };
    }

    static invalidKey() {
        return {
            msg: "Invalid password / persistent token.",
            code: 3
        };
    }

    static authFailed() {
        return {
            msg: "Authentication failed.",
            code: 4
        };
    }

    static userBanned(duration) {
        return {
            msg: "User has been banned.",
            code: 5,
            duration
        };
    }

    static noteNotExist() {
        return {
            msg: "Note does not exist.",
            code: 6
        };
    }
    
    static accessDenied() {
        return {
            msg: "You do not have permissions to do this.",
            code: 7
        };
    }

    static userExists() {
        return {
            msg: "User by that name exists.",
            code: 8
        };
    }

    static tooLarge() {
        return {
            msg: "Content is too large.",
            code: 9
        };
    }

    static alreadyStored() {
        return {
            msg: "Note is already stored",
            code: 10
        };
    }

    static invalidDataType(propName) {
        return {
            msg: "Invalid data type of '" + propName + "'",
            code: 11
        };
    }

    static invalidUsername() {
        return {
            msg: "Username consists only from characters a-z A-Z 0-9 and _",
            code: 12
        };
    }

    static tagNotExist() {
        return {
            msg: "Tag does not exist.",
            code: 13
        };
    }
    
    static noteAlreadyBookmarked() {
        return {
            msg: "Note id was already bookmarked.",
            code: 14
        };
    }

    static encryptionOnPublic() {
        return {
            msg: "Only invisible and private notes can be encrypted.",
            code: 14
        };
    }
};
