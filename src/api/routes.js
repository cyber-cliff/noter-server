/*

    Main routing module

*/

const express = require('express');

const authService = require("../services/auth");
const sitemapService = require("../services/sitemap");

const authHandlers = require("../handlers/authHandlers");
const userHandlers = require("../handlers/userHandlers");
const noteHandlers = require("../handlers/noteHandlers");
const tagHandlers = require("../handlers/tagHandlers");
const taskHandlers = require("../handlers/taskHandlers");
const infoHandlers = require("../handlers/infoHandlers");
const searchHandlers = require("../handlers/searchHandlers");
const discoverHandlers = require("../handlers/discoverHandlers");
const statsHandlers = require("../handlers/statsHandlers");

const errors = require("./errors");

let UN_ALLOWED_CHARACTERS = ['_'];
UN_ALLOWED_CHARACTERS = UN_ALLOWED_CHARACTERS.concat('1234567890'.split(""));
UN_ALLOWED_CHARACTERS = UN_ALLOWED_CHARACTERS.concat('abcdefghijklmnopqrstuvwxyz'.split(""));
UN_ALLOWED_CHARACTERS = UN_ALLOWED_CHARACTERS.concat('abcdefghijklmnopqrstuvwxyz'.toLocaleUpperCase().split(""));

const routes = [
    {
        path: 'auth/login',
        handler: authHandlers.login,
        dataStruct: {
            username: { required: true, type: 'username' },
            key: { required: true, type: 'string' }, // Can be password or PT (if PT, param is_persistent_token must be true!)
            is_persistent_token: { type: 'boolean' },
            generate_persistent_token: { type: 'boolean' }
        }
    },
    {
        path: 'auth/register',
        handler: authHandlers.register,
        dataStruct: {
            username: { required: true, type: 'username' },
            password: { required: true, type: 'string' }
        }
    },
    {
        path: 'auth/validate',
        handler: authHandlers.validateSession,
        dataStruct: {
            user_id: { required: true, type: 'string' },
            token: { required: true, type: 'string' }
        }
    },
    {
        path: 'auth/destroy',
        handler: authHandlers.destroySessions,
        authRequired: true,
        dataStruct: {}
    },

    {
        path: 'users/profile/get',
        handler: userHandlers.getUserProfile,
        dataStruct: {
            user_id: { required: true, type: 'string' }
        }
    },
    {
        path: 'users/notes/get',
        handler: userHandlers.getUserNotes,
        dataStruct: {
            user_id: { required: true, type: 'string' },
            tag_ids: { type: 'array' }
        }
    },

    {
        path: 'users/saved/get',
        handler: userHandlers.getSavedNotes,
        authRequired: true,
        dataStruct: {}
    },
    {
        path: 'users/saved/save',
        handler: userHandlers.bookmarkNote,
        authRequired: true,
        dataStruct: {
            note_id: { required: true, type: 'string' }
        }
    },
    {
        path: 'users/saved/remove',
        handler: userHandlers.unbookmarkNote,
        authRequired: true,
        dataStruct: {
            note_id: { required: true, type: 'string' }
        }
    },
    
    {
        path: 'notes/create',
        handler: noteHandlers.createNote,
        authRequired: true,
        dataStruct: {
            title: { type: 'string' },
            type: { type: 'string' }
        }
    },
    {
        path: 'notes/get',
        handler: noteHandlers.getNote,
        dataStruct: {
            note_id: { required: true, type: 'string' }
        }
    },
    {
        path: 'notes/update',
        handler: noteHandlers.update,
        authRequired: true,
        dataStruct: {
            note_id: { required: true, type: 'string' },
            note: { required: true, type: 'object' },
            access: { type: 'number' }
        }
    },
    {
        path: 'notes/delete',
        handler: noteHandlers.delete,
        authRequired: true,
        dataStruct: {
            note_id: { required: true, type: 'string' }
        }
    },

    {
        path: 'tags/get',
        handler: tagHandlers.get,
        authRequired: true,
        dataStruct: {}
    },
    {
        path: 'tags/create',
        handler: tagHandlers.create,
        authRequired: true,
        dataStruct: {
            name: { required: true, type: 'string' },
            color: { required: true, type: 'string' }
        }
    },
    {
        path: 'tags/delete',
        handler: tagHandlers.delete,
        authRequired: true,
        dataStruct: {
            tag_id: { required: true, type: 'string' }
        }
    },

    //? Tasks
    {
        path: 'tasks/create',
        handler: taskHandlers.createTask,
        authRequired: true,
        dataStruct: {
            name: { required: true, type: 'string' },
            timestamp: { required: true, type: 'number' }
        }
    },
    {
        path: 'tasks/get',
        handler: taskHandlers.getUserTasks,
        authRequired: true,
        dataStruct: {}
    },
    {
        path: 'tasks/update',
        handler: taskHandlers.update,
        authRequired: true,
        dataStruct: {
            task_id: { required: true, type: 'string' },
            task: { required: true, type: 'object' }
        }
    },
    {
        path: 'tasks/delete',
        handler: taskHandlers.deleteTask,
        authRequired: true,
        dataStruct: {
            task_id: { required: true, type: 'string' }
        }
    },

    //? Other
    {
        path: 'stats/get',
        handler: statsHandlers.getStats,
        dataStruct: {}
    },

    {
        path: 'search',
        handler: searchHandlers.search,
        dataStruct: {
            phrase: { required: true, type: 'string' },
            users: { type: 'boolean' },
            published_notes: { type: 'boolean' },
            user_notes: { type: 'boolean' }
        }
    },

    {
        path: 'discover/published',
        handler: discoverHandlers.publishedNotes,
        dataStruct: {}
    },
    {
        path: 'discover/news',
        handler: discoverHandlers.newsNotes,
        dataStruct: {}
    },

    {
        path: 'info/about',
        handler: infoHandlers.getInfo,
        dataStruct: {}
    }
];

module.exports = class {
    static init(app) {
        for (const route of routes) {
            app.post('/api/v2/' + route.path, async (req, res) => {
                await this.processRequest(route, req, res);
            });
        }

        app.get('/api/v2/sitemap.xml', async (req, res) => {
            res.set('Content-Type', 'text/xml');
            res.send(sitemapService.generateSitemap());
        });
    }

    static async processRequest(route, req, res) {
        const startTime = new Date().getTime();

        const data = req.body.data;

        // * Missing data (data object must be here even when empty!)
        if (!data) {
            await res.json({
                success: false,
                error: errors.missingDetails('!data')
            });
            return;
        }

        const auth = req.body.auth;

        if (route.authRequired && !auth) {
            await res.json({
                success: false,
                error: errors.authFailed()
            });
            return;
        }


        if (auth) {            
            const isSessionValid = authService.validateSession({
                user_id: auth.user_id,
                token: auth.token,
                ua: req.header('User-Agent')
            }, auth.disable_active_time);

            if (!isSessionValid) {
                await res.json({
                    success: false,
                    error: errors.authFailed()
                });
                return;
            }
        }

        console.log('Auth check complete!');

        const validatorResponse = this.checkData(route.dataStruct, data);

        if (!validatorResponse.valid) {
            if (validatorResponse.error === 'required') {
                await res.json({
                    success: false,
                    error: errors.missingDetails(validatorResponse.prop)
                });
            } else if (validatorResponse.error === 'type') {
                await res.json({
                    success: false,
                    error: errors.invalidDataType(validatorResponse.prop)
                });
            }
            return;
        }

        let handlerResponse;
        try {
            const handler = route.handler;
            handlerResponse = await handler(data, auth, req);
        } catch (e) {
            if (e.msg && e.code) {
                await res.json({
                    success: false,
                    error: e
                });
            } else {
                await res.json({
                    success: false,
                    error: errors.internalError("Handler threw up: " + e)
                });
            }

            return;
        }

        await res.json({
            success: true,
            data: (handlerResponse) ? handlerResponse.data : {}
        });

        const deltaTime = new Date().getTime() - startTime;

        console.log(`[i] Resolving ${route.path} took ${deltaTime}ms`);

        return;
    }

    static checkData(dataStruct, data) {
        for (const propName of Object.keys(dataStruct)) {
            const structProp = dataStruct[propName];

            const dataProp = data[propName];

            if (structProp.required && !dataProp) { // If the prop in dataStruct is required, and data does not have it, we send it to the void.
                return { valid: false, prop: propName, error: 'required' };
            }

            if (dataProp === undefined) { continue; } // Don't check data types if the prop does not exist
            //* Now check the data type
            // TODO: refactor this
            if (['string', 'boolean', 'number'].includes(structProp.type)) {
                if (typeof (dataProp) !== structProp.type) {
                    return { valid: false, prop: propName, error: 'type' };
                }
            } else if (structProp.type === 'array') {
                if (dataProp.constructor !== Array) {
                    return { valid: false, prop: propName, error: 'type' };
                }
            } else if (structProp.type === 'username') {
                if (!module.exports.isUsernameValid(dataProp)) {
                    return { valid: false, prop: propName, error: 'type' };
                }
            }
        }

        return { valid: true };
    }

    // Makes sure the username is only a-z 0-9 and with "_"
    static isUsernameValid(username) {
        for (const letter of username) {
            if (!UN_ALLOWED_CHARACTERS.includes(letter)) {
                return false;
            }
        }

        return true;
    }
};
