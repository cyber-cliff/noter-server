
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const statsMiddleware = require('./middlewares/stats');

const routes = require("./routes");

module.exports = function() {
    const app = express();
    const port = process.env.PORT || 4068; // TODO: add this to the config

    app.use(bodyParser());
    app.use(cors());
    app.use(statsMiddleware.mv());

    app.use(express.static(__dirname + '../../../public'));

    routes.init(app); // Initialize API routing    

    app.listen(port, () => console.log(`Server listening on ${port}`));

    return app;
};
