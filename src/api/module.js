const initApi = require("./init");

let app;

module.exports.init = async function() {
    app = await initApi();
};