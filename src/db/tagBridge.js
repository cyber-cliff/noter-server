
const ObjectId = require("mongodb").ObjectID;
const dbModule = require("./index");
const isReady = dbModule.isReady;

module.exports = class {
    static async getTagsByUserId(userId) {
        if (!isReady()) { throw ("Database not ready."); }

        let tags;
        try {
            tags = await dbModule.db.collection("tags").find({ author: userId }).toArray();
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }

        return tags;
    }

    static async createTag(tag) {
        if (!isReady()) { throw ("Database not ready."); }
    
        let res;
        try {
            res = await dbModule.db.collection("tags").insertOne(tag);
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }

        return res.ops[0];
    }

    static async deleteTag(tagId) {
        if (!isReady()) { throw ("Database not ready."); }
    
        let res;
        try {
            res = await dbModule.db.collection("tags").deleteOne({_id: ObjectId(tagId)});
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }
    }

    static async getTagsById(idArray) {
        if (!isReady()) { throw ("Database not ready."); }

        let i = 0;
        for (const id of idArray) {
            idArray[i] = ObjectId(id);
            i++;
        }

        let tags;
        try {
            tags = await dbModule.db.collection("tags").find({ _id:  {$in: idArray} }).toArray();
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }
        return tags;
    }
};