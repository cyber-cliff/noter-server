const ObjectId = require("mongodb").ObjectID;
const dbModule = require("./index");
const isReady = dbModule.isReady;

module.exports = class {
    static async createTask(task) {
        if (!isReady()) { throw ("Database not ready."); }

        let res;
        try {
            res = await dbModule.db.collection("tasks").insertOne(task);
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }

        return res.ops[0]._id.toString();
    }

    static async getUserTasks(userId) {
        if (!isReady()) { throw ("Database not ready."); }

        let tasks;
        try {
            tasks = await dbModule.db.collection("tasks").find({ author: userId }).toArray();
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }

        return tasks;
    }

    static async getTasksById(taskIds) {
        if (!isReady()) { throw ("Database not ready."); }

        taskIds = taskIds.map(taskId => {
            return ObjectId(taskId);
        });

        let tasks;
        try {
            tasks = await dbModule.db.collection("tasks").find(
                { _id: { $in: taskIds } }
            ).toArray();
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }

        return tasks;
    }

    static async updateTask(taskId, task) {
        if (!isReady()) { throw ("Database not ready."); }

        let res;
        try {
            res = await dbModule.db.collection("tasks").updateOne({ _id: ObjectId(taskId) }, { $set: task });
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }

        return res.result;
    }

    static async deleteTask(taskId) {
        if (!isReady()) { throw ("Database not ready."); }

        try {
            await dbModule.db.collection("tasks").deleteOne({ _id: ObjectId(taskId) });
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }

        return;
    }

};