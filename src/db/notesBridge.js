const ObjectId = require("mongodb").ObjectID;
const dbModule = require("./index");
const isReady = dbModule.isReady;

module.exports.createNote = async function (note) {
    if (!isReady()) {
        throw ("Database not ready.");
    }

    let res;
    try {
        res = await dbModule.db.collection("notes").insertOne(note);
    } catch (e) {
        throw ("Failed to execute db query: " + e);
    }

    return res.ops[0]._id.toString();
};

module.exports.getNotesById = async function (noteIds, omitDocBodies) {
    if (!isReady()) {
        throw ("Database not ready.");
    }

    noteIds = noteIds.map(noteId => {
        return ObjectId(noteId);
    });

    let projection = {};
    if (omitDocBodies) {
        projection['doc.body'] = 0;
    }

    let notes;
    try {
        notes = await dbModule.db.collection("notes").find(
            {_id: {$in: noteIds}}
        ).project(projection).toArray();
    } catch (e) {
        throw ("Failed to execute db query: " + e);
    }

    return notes;
};

module.exports.getNotesByUserId = async function (userId, getInvisibleNotes, tagIds, omitDocBodies) {
    if (!isReady()) {
        throw ("Database not ready.");
    }

    let query = {$and: [{author: userId}]};

    // Now we add requirements
    if (!getInvisibleNotes) { // If the user doesn't have access to perm 0 and 1 notes
        query.$and.push({access: {$in: [2, 3]}}); // We require all fetched notes to have access 2 / 3
    }

    if (!tagIds || tagIds.length === 0) {
        query.$and.push({tags: {$eq: []}});
    } else { // If we have tagIds, we select the notes with them
        query.$and.push({tags: {$all: tagIds}});
    }

    let projection = {};
    if (omitDocBodies) {
        projection['doc.body'] = 0;
    }

    let notes;
    try {
        notes = await dbModule.db.collection("notes").find(query).project(projection).sort({modified_at: -1}).toArray();
    } catch (e) {
        throw ("Failed to execute db query: " + e);
    }

    return notes;
};

module.exports.updateNote = async function (noteId, note) {
    if (!isReady()) {
        throw ("Database not ready.");
    }

    let res;
    try {
        res = await dbModule.db.collection("notes").updateOne({_id: ObjectId(noteId)}, {$set: note});
    } catch (e) {
        throw ("Failed to execute db query: " + e);
    }

    return res.result;
};

module.exports.deleteNote = async function (noteId) {
    if (!isReady()) {
        throw ("Database not ready.");
    }

    try {
        await dbModule.db.collection("notes").deleteOne({_id: ObjectId(noteId)});
    } catch (e) {
        throw ("Failed to execute db query: " + e);
    }

    return;
};

module.exports.deleteTagFromNotes = async function (tagId) {
    if (!isReady()) {
        throw ("Database not ready.");
    }

    let res;
    try {
        res = await dbModule.db.collection("notes").updateMany({tags: tagId}, {$pull: {tags: tagId}});
    } catch (e) {
        throw ("Failed to execute db query: " + e);
    }

    return;
};

module.exports.getRecentPublishedNotes = async function () {
    if (!isReady()) {
        throw ("Database not ready.");
    }

    let notes;
    try {
        notes = await dbModule.db.collection("notes")
            .find({access: 3})
            .project({'doc.body': 0})
            .sort({modified_at: -1})
            .limit(10)
            .toArray();
    } catch (e) {
        throw ("Failed to execute db query: " + e);
    }

    return notes;
};

module.exports.getAllPublishedNotes = async function () {
    if (!isReady()) {
        throw ("Database not ready.");
    }

    let notes;
    try {
        notes = await dbModule.db.collection("notes")
            .find({access: 3})
            .project({'_id': 1})
            .toArray();
    } catch (e) {
        throw ("Failed to execute db query: " + e);
    }

    return notes;
};

module.exports.getRecentNewsNotes = async function () {
    if (!isReady()) {
        throw ("Database not ready.");
    }

    let notes;
    try {
        notes = await dbModule.db.collection("notes")
            .find({news_post: {$exists: true}})
            .project({'doc.body': 0})
            .sort({modified_at: -1})
            .limit(3)
            .toArray();
    } catch (e) {
        throw ("Failed to execute db query: " + e);
    }

    return notes;
};
