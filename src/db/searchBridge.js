const dbModule = require("./index");
const isReady = dbModule.isReady;

module.exports = class {
    static async searchNotes(phrase) {
        if (!isReady()) { throw ("Database not ready."); }

        let notes;
        try {
            notes = await dbModule.db.collection("notes").find({$text: { $search: phrase}, access: 3}).sort({modified_at: -1}).limit(10).toArray();
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }

        return notes;
    }
    static async searchUserNotes(phrase, userId) {
        if (!isReady()) { throw ("Database not ready."); }

        let notes;
        try {
            notes = await dbModule.db.collection("notes").find({$text: { $search: phrase}, author: userId}).sort({modified_at: -1}).limit(10).toArray();
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }

        return notes;
    }
    static async searchUsers(phrase) {
        if (!isReady()) { throw ("Database not ready."); }

        let users;
        try {
            users = await dbModule.db.collection("users").find({username: {$regex: phrase, $options: "i"}}).limit(10).project({persistent_tokens: 0, password_hash: 0}).toArray();
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }

        return users;
    }
};
