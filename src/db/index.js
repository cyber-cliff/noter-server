
const MongoClient = require('mongodb').MongoClient;

let client, db;

// Status:
// 0 - initial: no even attempted to connect yet
// 1 - connected
// 2 - failed to connect in the first place
// 3 - got disconnected
let dbConnStatus = 0;

// * Connection management methods

module.exports.init = async function() {
    try { // To connect to Wanilla mongoDB
        console.log("[DB] Trying to connect to db...");
        connectedClient = await MongoClient.connect(process.env.N_DBURI, {
            bufferMaxEntries: 0, // If there's an error, throw it instead of waiting on reconnect!
            useNewUrlParser: true,
            useUnifiedTopology: true
        });

        client = connectedClient;
        db = client.db('noter');
        
        module.exports.client = client;
        module.exports.db = db;

        db.on('close', () => {
            console.log("Connection closed for some reason. Will try to reconnect.");
            dbConnStatus = 3; // Got disconnected.
            module.exports.reconnectToDB();
        });
        db.on('reconnect', () => {
            console.log("Reconnected to db.");
            dbConnStatus = 1; // Connected. again.
        });

        console.log("[DB] Connected to the database");
        dbConnStatus = 1; // Connected.
        return client;
    } catch (e) {
        dbConnStatus = 2; // Failed to connect.

        module.exports.reconnectToDB();
    }
};

module.exports.reconnectToDB = function() {
    client = false;
    db = false;

    console.error("Going to reconnect in 5 seconds");

    setTimeout(module.exports.init, 5000);
    return;
};

module.exports.isReady = function () {
    switch (dbConnStatus) {
        case 0:
            console.log("[DBSTAT] Database connection not yet tried");
            return false;
        case 1:
            return true;
        case 2:
            console.log("[DBSTAT] Failed to connect to database. Retrying in the background.");
            return false;
        case 3:
            console.log("[DBSTAT] Got disconnected from the database. Retrying in the background.");
            return false;
        default:
            throw ("Unknown dbConnStat:" + e);
    }
};