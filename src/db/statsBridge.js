
const dbModule = require("./index");
const isReady = dbModule.isReady;

module.exports = class {
    static async getCounts() {
        if (!isReady()) { throw ("Database not ready."); }

        let noteCount;
        try {
            noteCount = await dbModule.db.collection("notes").countDocuments({});
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }
        let userCount;
        try {
            userCount = await dbModule.db.collection("users").countDocuments({});
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }
        let taskCount;
        try {
            taskCount = await dbModule.db.collection("tasks").countDocuments({});
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }

        return {
            noteCount,
            userCount,
            taskCount
        };
    }

    static async getLastStatsEntry() {
        if (!isReady()) { throw ("Database not ready."); }

        let statusEntries;
        try {
            statusEntries = await dbModule.db.collection("stats")
                .find({})
                .sort({timestamp: -1})
                .limit(1)
                .toArray();
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }

        return statusEntries[0];
    }

    static async getStats(limit) {
        if (!isReady()) { throw ("Database not ready."); }

        let statusEntries;
        try {
            statusEntries = await dbModule.db.collection("stats")
                .find({})
                .sort({timestamp: -1})
                .limit(limit)
                .toArray();
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }

        return statusEntries;
    }

    static async insertStatsSnap(statsSnap) {
        if (!isReady()) { throw ("Database not ready."); }
    
        let res;
        try {
            res = await dbModule.db.collection("stats").insertOne(statsSnap);
        } catch (e) {
            throw ("Failed to execute db query: " + e);
        }
    
        return;
    }
};