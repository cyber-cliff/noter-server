
const dbModule = require("./index");
const isReady = dbModule.isReady;
const ObjectId = require("mongodb").ObjectID;

// Stores user object in database
module.exports.registerUser = async function (user) {
    if (!isReady()) { throw ("Database not ready."); }

    try {
        await dbModule.db.collection("users").insertOne(user);
    } catch (e) {
        throw ("Failed to insert the new user: " + e);
    }

    return true;
};

// Gets user objects by ids in an array
module.exports.getUsersById = async function (userIds) {
    if (!isReady()) { throw ("Database not ready."); }

    userIds = userIds.map(userId => {
        return ObjectId(userId);
    });

    let users;
    try {
        users = await dbModule.db.collection("users")
            .find({_id: { $in: userIds}})
            .toArray();
    } catch (e) {
        throw ("DB query error: " + e);
    }

    return users;
};

// Gets the first (and only) user with a username specified
module.exports.getUserByUsername = async function (username) {
    if (!isReady()) { throw ("Database not ready."); }

    let users;
    try {
        users = await dbModule.db.collection("users").find({username}).toArray();
    } catch (e) {
        throw ("Failed to get user: " + e);
    }

    if (users.length <= 0) {
        return false;
    }

    return users[0];
};

// Pushes PT object to PTs of a user specified by user id
module.exports.storePersistentToken = async function (userId, persistentToken) {
    if (!isReady()) { throw ("Database not ready."); }

    try {
        await dbModule.db.collection("users").updateOne({ _id: ObjectId(userId)}, { $push: { persistent_tokens: persistentToken } });
    } catch (e) {
        throw ("Failed to insert PT: " + e);
    }

    return true;
};

module.exports.deauth = async function (userId) {
    if (!isReady()) { throw ("Database not ready."); }

    try {
        await dbModule.db.collection("users").updateOne({_id: ObjectId(userId)}, {$set: {persistent_tokens: []}});
    } catch (e) {
        throw ("Failed to deauth user: " + e);
    }

    return true;
};

module.exports.pushBookmarkedNote = async function (userId, noteId) {
    if (!isReady()) { throw ("Database not ready."); }

    try {
        await dbModule.db.collection("users").updateOne({ _id: ObjectId(userId)}, { $push: { saved_notes: noteId } });
    } catch (e) {
        throw ("Failed to insert bookmarked note: " + e);
    }

    return true;
};

module.exports.removeBookmarkedNote = async function (userId, noteId) {
    if (!isReady()) { throw ("Database not ready."); }

    try {
        await dbModule.db.collection("users").updateOne({ _id: ObjectId(userId)}, { $pull: { saved_notes: noteId } });
    } catch (e) {
        throw ("Failed to pull bookmarked note: " + e);
    }

    return true;
};

module.exports.updateActiveAt = async function(userId, reset) {
    if (!isReady()) { throw ("Database not ready."); }

    try {
        if (reset) {
            await dbModule.db.collection("users").updateOne({ _id: ObjectId(userId)}, { $set: { active_at: 0 } });
        } else {
            await dbModule.db.collection("users").updateOne({ _id: ObjectId(userId)}, { $set: { active_at: new Date().getTime() } });
        }
    } catch (e) {
        throw ("Failed to update active at for user: " + e);
    }

    return true;
};