

module.exports = class {
    static userProfile(user) {
        return {
            id: user._id,
            username: user.username || '',
            created_at: user.created_at || 0,
            active_at: user.active_at,
            description: user.description || '',
            badges: user.badges || []
        };
    } 
};