module.exports = class {
    static task(task) {        
        return {
            id: task._id,
            author: task.author,
            name: task.name,
            timestamp: task.timestamp,
            completed: task.completed,
            tags: task.tags
        };
    } 
};