

module.exports = class {
    static noteThumbnail(note) {        
        return {
            id: note._id,
            author: note.author,
            title: note.title,
            tags: note.tags,
            created_at: note.created_at,
            modified_at: note.modified_at,
            type: note.doc.head.type || 'text',
            access: note.access,
            encrypted: !!note.doc.head.encryption,
            news_post: note.news_post
        };
    } 
    static note(note) {        
        return {
            id: note._id,
            author: note.author,
            title: note.title,
            tags: note.tags,
            created_at: note.created_at,
            modified_at: note.modified_at,
            access: note.access,
            encrypted: !!note.doc.head.encryption,
            news_post: note.news_post,
            doc: note.doc
        };
    } 
};
