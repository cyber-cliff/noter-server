

module.exports = class {
    static tag(tag) {        
        return {
            id: tag._id,
            author: tag.author,
            name: tag.name,
            color: tag.color,
            public: tag.public
        };
    } 
};