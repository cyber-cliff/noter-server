

module.exports = class {
    static statEntry(stat) {        
        return {
            timestamp: stat.timestamp,
            counts: stat.counts,
            requests: stat.requests
        };
    } 
};