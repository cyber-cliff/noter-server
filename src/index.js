console.log("Noter Server. (c) FajsiEx 2019 - Licensed under MIT license.");

(async ()=> {
    await require("./db/index").init();
    await require("./api/module").init();
    require("./services/stats").initializeTimeout(); // Kickstart the loop
    require("./services/sitemap").initializeTimeout(); // Kickstart the loop
    //require("./services/gdrive").init();
})();
