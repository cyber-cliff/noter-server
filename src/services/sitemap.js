const notesBridge = require("../db/notesBridge");

let publishedNotes = [];

module.exports = class {
    static initializeTimeout() {
        this.storePublishedNoteIds(); // Starts a loop with a timeout
    }

    static async storePublishedNoteIds() {
        const notes = await notesBridge.getAllPublishedNotes();

        for (const note of notes) {
            publishedNotes.push(note._id);
        }

        console.log("All published notes ids:", publishedNotes);

        setTimeout(async () => {
            this.storePublishedNoteIds();
        }, 60 * 60 * 1000);
    }

    static generateSitemap() {
        let urlEntries = [];

        for (const noteId of publishedNotes) {
            urlEntries.push(`<url><loc>https://noter.ml/n/${noteId}</loc></url>`);
        }

        const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ${urlEntries.join('\n')}
        </urlset>`;

        console.log(sitemap);

        return sitemap;
    }
};
