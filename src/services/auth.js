/* 

    Authorization service

    Handles login, registering, tokens, and things like that

*/

const bcrypt = require("bcrypt");

const usersDBBridge = require("../db/usersBridge");

let sessions = [
    // TEMPLATE
    {
        token: "test",
        user_id: "5asd6d8a912bdff5",
        ip: "::1",
        ua: "IDontExist/1.1",
    }
];

module.exports = class {
    static async storeSession({userId, ua, generate_persistent_token}) {
        const token = this.generateToken();
        sessions.push({
            token,
            user_id: userId,
            ip: '',
            ua
        });

        let generatedPT;
        if (generate_persistent_token) {
            generatedPT = this.generateToken();

            const ptHash = await bcrypt.hash(generatedPT, 6);
            const pt = {
                hash: ptHash,
                createdAt: new Date().getTime()
            };

            usersDBBridge.storePersistentToken(userId, pt);
        }

        return {
            token,
            generatedPT
        };
    }

    static validateSession({user_id, token, ua}, disableActiveTime) {
        const targetSession = sessions.filter((session) => { return session.token == token; })[0];
        if (!targetSession) {console.log('[SESS_INV] token'); return false;}
        if (targetSession.user_id != user_id) {console.log('[SESS_INV] user_id'); return false;}
        if (targetSession.ua != ua) {console.log('[SESS_INV] ua'); return false;}

        usersDBBridge.updateActiveAt(user_id, disableActiveTime);

        return true; // After all these checks succeed
    }

    static deleteSessionByUserId(userId) {
        sessions = sessions.filter((session) => { return session.user_id !== userId; });
    }

    static generateToken() {
        const pow = Math.pow(10, 9); // TODO: Move the second value to the config

        let generatedToken = "";
        do {
            generatedToken = "";
            for(let i=0; i<4; i++) generatedToken += Math.floor(Math.random() * 9*pow + pow).toString(32);
        }while(
            sessions.filter(session=> { return session.token == generatedToken; })[0] ||
            generatedToken.length != 4*7 // TODO: Move the required length value to config
        );

        return generatedToken;
    }
};