
const requestsMiddleware = require('../api/middlewares/stats');
const statsBridge = require("../db/statsBridge");

const MAXIMUM_TIME_BETWEEN_ENTRIES = 1*60*60*1000; // (1hour) The maximum time before entry is automatically made
const MINIMUM_TIME_BETWEEN_ENTRIES = 60*1000; // Minimum time to retry (so the db is not spammed)

module.exports = class {
    static async initializeTimeout() {        
        const lastStatusEntry = await statsBridge.getLastStatsEntry();

        if (!lastStatusEntry) {
            console.warn('No previous statsSnap found. Creating one right now!');
            this.snapStats();
            return;
        }

        const lastEntryTimestamp = lastStatusEntry.timestamp;

        const lastEntryDelta = new Date().getTime() - lastEntryTimestamp;

        let timeoutNeeded = MAXIMUM_TIME_BETWEEN_ENTRIES - lastEntryDelta;

        if (timeoutNeeded <= MINIMUM_TIME_BETWEEN_ENTRIES) {
            timeoutNeeded = MINIMUM_TIME_BETWEEN_ENTRIES;
        }

        console.log('Milliseconds until next snap: ' + timeoutNeeded);

        setTimeout(async ()=>{
            await this.snapStats();
        }, timeoutNeeded);
    }

    static async snapStats() {
        const counts = await statsBridge.getCounts();
        const requests = requestsMiddleware.getStats();
        

        const statsSnap = {
            timestamp: new Date().getTime(),
            counts,
            requests
        };

        await statsBridge.insertStatsSnap(statsSnap);

        requestsMiddleware.resetStats();
        // Finally we init another timeout so the cycle continues
        this.initializeTimeout();
    }
};